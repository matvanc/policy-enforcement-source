#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <string>
#include <cstring>
#include <vector>
#include <map>
#include <list>

#include "logging.h"
#include "utilities.h"
#include "taint_map.h"

using namespace std;

#define LOG_FILE "/sdcard/pe_log.txt"
#define LOG_FILE_DEBUG "/sdcard/pe_log_debug.txt"
#define LOG_FILE_ERROR "/sdcard/pe_log_error.txt"
#define LOG_FILE_TAINTMAP "/sdcard/pe_log_taintmap.txt"

#define LOG_TAG "pe"
#define LOG_TAG_DEBUG "pe_debug"

#define START_FILE_MARKER "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n"
#define END_FILE_MARKER ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n"

#include "logfile.h"

static bool first_run = true;

#define BUF_SIZE 2048
char buf[BUF_SIZE];

#define BUF_CONT_SIZE 3 * 448000
char buf_cont[BUF_CONT_SIZE];

/* CONFIGURATION OF LOGGING */
const bool SHOW_FILE_CONTENT = true;

/* END CONFIGURATION OF LOGGING */


void write_to_file(const char* fname, const char * str, ...) {
	FILE* flog = fopen(fname, "a");

    va_list argptr;
    va_start(argptr, str);
    vfprintf(flog, str, (__va_list) argptr);
    va_end(argptr);

	fclose(flog);
}



void log_to_file(const char* fname, const char * str, ...)
{
	/* INITIALIZATIONS FOR TESTS */


	if (first_run) {
		FILE* flog = fopen(LOG_FILE, "w");
		fclose(flog);
		flog = fopen(LOG_FILE_DEBUG, "w");
		fclose(flog);
		flog = fopen(LOG_FILE_TAINTMAP, "w");
		fclose(flog);




		first_run = false;
	}

	FILE* flog = fopen(fname, "a");

	fprintf(flog, "%s", print_time());

    va_list argptr;
    va_start(argptr, str);
    vfprintf(flog, str, (va_list) argptr);
    va_end(argptr);

    fprintf(flog, "\n");

    fflush(flog);

	fclose(flog);
}

void log_buffer(const void *buffer, size_t length) {
	if (!SHOW_FILE_CONTENT) {
		return;
	}

	write_to_file(LOG_FILE, "%s", START_FILE_MARKER);

	FILE* flog = fopen(LOG_FILE, "a");
	for (int i = 0; i < length; i++) {

		char c = *((char *) buffer + i);
		int c_int = (int) c;

		if (c_int >= 65 && c_int <= 122) {
			fprintf(flog, "%c", c);
		}
		else {
			fprintf(flog, ".%d", c_int);
		}
	}
	fclose(flog);

	write_to_file(LOG_FILE, "\n%s", END_FILE_MARKER);
}

void buffer_to_string(char *dst, int dst_size, const void *buffer, int length) {
	string str = "";
	for (int i = 0; i < length; i++) {

		const char c = *((const char *) buffer + i);
		int ord = (int) c;
		char c_int[10];
		sprintf(c_int, "%02X", (int) c);

		if (ord >= 32 && ord <= 126) {
			str += "'";
			str += c;
			str += " ";
		}
		else {
			str += "`";
			str += c_int;
		}
	}

	if (str.length() > (dst_size - 3)) {
		string str2 = str.substr(0, dst_size - 3);
		str2 += "\0";
		strncpy(dst, str2.c_str(), dst_size);
	}
	else {
		str += "\0";
		strncpy(dst, str.c_str(), dst_size);
	}




	/*
	if (str.length() > dst_size) {
		str = str.substr(0, dst_size);
	}
	//strncpy(dst, str2.c_str(), dst_size - 1);

	snprintf(dst, dst_size, "%s\n", str.c_str());
	*/
}

void log_open(int result, const char *file, int flags, int mode) {
	//LOG_TO_CAT("%s open: %s, result: %d, file: %s, flags: %d, mode %d", print_time(), pe_get_file_name(result), result, file, flags, mode);
	log_to_file(LOG_FILE, "open: %s, fd: %d, flags: %d, mode %d\n", file, result, flags, mode);
}

void log_close(int result, int fd) {
	//LOG_TO_CAT("%s write: %s, result: %d, file: %d, count: %d,", print_time(), pe_get_file_name(result), result, file, count);
	char filename[255];
	//pe_get_file_name(fd, filename);


	string path = "nenasiel";
	if ( fileMap.find(fd) != fileMap.end() ) {
		path = fileMap[fd]->path;
	}



	log_to_file(LOG_FILE, "close: %s(%s), fd: %d, result: %d\n", path.c_str(), filename, fd, result);
}

void log_read(int result, int file, void *buffer, size_t length) {
	//LOG_TO_CAT("%s read: %s, result: %d, file: %d, length %d", print_time(), pe_get_file_name(result), result, file, length);

	char filename[255];
	char filename2[255];
	//filemap_getname(file, filename2);
	//pe_get_file_name(file, filename);

	snprintf(buf, BUF_SIZE, "read: (%s), fd: %d, count: %d/%d, block: %u-%u\n", filename, file, result, length, (unsigned int) buffer, ((unsigned int) buffer) + result);
	//buffer_to_string(buf_cont, BUF_CONT_SIZE, buffer, result);

	log_to_file(LOG_FILE, "%s%s%s%s", buf, START_FILE_MARKER, buf_cont, END_FILE_MARKER);
}

void log_write(int result, int file, const void *buffer, size_t count) {
	//LOG_TO_CAT("%s write: %s, result: %d, file: %d, count: %d,", print_time(), pe_get_file_name(result), result, file, count);

	char filename[255];
	char filename2[255];
	//filemap_getname(file, filename2);
	//pe_get_file_name(file, filename);

	snprintf(buf, BUF_SIZE, "write: (%s), fd: %d, count: %d/%d, block: %u-%u\n", filename, file, result, count, (unsigned int) buffer, ((unsigned int) buffer) + result);
	//buffer_to_string(buf_cont, BUF_CONT_SIZE, buffer, result);

	log_to_file(LOG_FILE, "%s%s%s%s", buf, START_FILE_MARKER, buf_cont, END_FILE_MARKER);
}

void log_debug(const char *str, ...) {
	FILE* flog = fopen(LOG_FILE_DEBUG, "a");

	fprintf(flog, "%s", print_time());

	va_list argptr;
	va_start(argptr, str); //Requires the last fixed parameter (to get the address)
	vfprintf(flog, str, (__va_list)argptr);
	va_end(argptr);

	fprintf(flog, "\n");

	fclose(flog);
}

void make_snapshot(string text) {
  log_to_file(LOG_FILE_DEBUG, "\n\n\n  *** SNAPSHOT (%s) ***", text.c_str());
  FileInfo::printAll();
  MemBlock::printAll();
  printPrivateFiles();
  printTaintedFiles();
}
