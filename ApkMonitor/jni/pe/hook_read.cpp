#include <stdio.h>
#include <string.h>

#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <list>

#include "taint_map.h"
#include "logging.h"
#include "utilities.h"
#include "sha256.h"

using namespace std;

void handle_before_read(int file, void *buffer, size_t length) {
  //log_read(0, file, buffer, length);
}

void handle_after_read(int result, int file, void *buffer, size_t length) {
  //log_read(result, file, buffer, length);
  
  unsigned char hash[32];
  SHA256_CTX ctx;  
  sha256_init(&ctx);
  sha256_update(&ctx, (unsigned char *) buffer, result);
  sha256_final(&ctx, hash);

  MemBlock *memBlock;
  //while ( (memBlock = MemBlock::isOverlapping(buffer, result)) != NULL ) {
  //  MemBlock::cutBlock(memBlock, buffer, result);
  //}

  if (fileMap.find(file) == fileMap.end()) { //cant continue if this is not valid value
 	  return;
   }

  if (fileMap.find(file) != fileMap.end() && isTaintedFile(fileMap[file]->path)) { //file not found in fileInfo database
	MemBlock *block = new MemBlock;

	block->start = (unsigned long) buffer;
	//block->start = 4294959600; //TESTING FILE-TAINTING

	block->size = result;
	block->fdSrc = file;
	block->fpSrc = TaintedFile::create(fileMap[file]->path);
	strncpy((char *) block->hash, (char *) hash, SIZE_OF_HASH);

	MemBlock::createSmallBlocks(buffer, result, (unsigned long) buffer);

	taintMap.push_back(block);
  }
  make_snapshot("AFTER after_read");
}
