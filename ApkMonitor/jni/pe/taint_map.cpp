#include <stdio.h>
#include <string.h>

#include <string>
#include <vector>
#include <list>
#include <map>

#include "logging.h"
#include "utilities.h"
#include "sha256.h"

using namespace std;

// if 0 content of file (small blocks) is not used, otherwise used this size for division
#define SMALL_BLOCK_SIZE 4

// if 0 hashes are not used, otherwise used only when memory block is equally sized or bigger
#define MIN_BLOCK_SIZE (SMALL_BLOCK_SIZE * 4)

#define SIZE_OF_HASH 32





class TaintedFile {
public:
  unsigned long long time; //for testing and further extensions with tracking all paths
  string path;

  static TaintedFile create(string path);
  static bool foundInVector(string path, vector<TaintedFile> *vec);
};

unsigned long long globalTime = 0;

TaintedFile TaintedFile::create(string path) {
  TaintedFile *result = new TaintedFile;
  
  result->path = path;
  result->time = globalTime;

  globalTime = globalTime + 1;
  
  return *result;
}

bool TaintedFile::foundInVector(string path, vector<TaintedFile> *vec) {
  vector<TaintedFile>::iterator it;
  for (it = vec->begin(); it < vec->end(); it++) {
    if (it->path == path) {
      return true;
    }
  }
  
  return false;
}





vector<TaintedFile> privateFiles; // absolute paths of user-selected files, protected against removal!!!

vector<TaintedFile> taintedFiles; //absolute paths of newly tainted files

bool isMasked = false;
  
//contains part if information
class SmallBlock { //could be string, but this is more memory efficient and faster for processing
public:
  unsigned long start; //id of block
  char block[SMALL_BLOCK_SIZE];
};

class MemBlock { //methods move to class and rewrite to static
  public:
	unsigned long start; /* start address */
	int size; /* size of memory block */
	int fdSrc; /* source file descriptor */
	TaintedFile fpSrc; /* full path of source file - do not have to be but verify! */
	unsigned char hash[SIZE_OF_HASH + 1]; /* counted hash, 32B including '\0' character */
	

	static MemBlock *add(int result, int file, void *buffer);
	static MemBlock *isOverlapping(const void *buffer, int readSize);
	static void untaint(int fd);
	static void createSmallBlocks(const void *buffer, int readSize, unsigned long start);
	static void cutBlock(MemBlock *block, const void *buffer, int size);
	static void printAll();
};

vector<MemBlock *> taintMap;



vector<SmallBlock *> smallBlocks;






class FileInfo {
  public:
	int mode;
	int flags;
	string path;
	
	static void add(int fd, const char *file, int flages, int mode);
	static void printAll();
};

map<int, FileInfo *> fileMap; // key is like indexted fd parameter





void FileInfo::add(int fd, const char *file, int flags, int mode) {
	FileInfo *fileInfo = new FileInfo;
	
	fileInfo->path = string(file);
	fileInfo->flags = flags;
	fileInfo->mode = mode;

	fileMap[fd] = fileInfo;

	//map<int, FileInfo *>::iterator it = fileMap.begin();
	//fileMap.insert(it, pair<int, FileInfo *>(fd, fileInfo));
}

void FileInfo::printAll() {
	map<int, FileInfo *>::iterator it;
	for (it = fileMap.begin(); it != fileMap.end(); it++) {
		int fd = it->first;
		FileInfo *file = it->second;
		log_to_file(LOG_FILE_DEBUG, "fd: %u, mode: %d, flags: %d, path: %s", fd, file->mode, file->flags, file->path.c_str());

	};
}





MemBlock *MemBlock::add(int result, int file, void *buffer)
{
  MemBlock *block = new MemBlock;

  block->start = (unsigned long) buffer;
  //block->start = 4294959600; //TESTING FILE-TAINTING

  block->size = result;
  block->fdSrc = file;
  
  unsigned char hash[32];
  SHA256_CTX ctx;  
  sha256_init(&ctx);
  sha256_update(&ctx, (unsigned char *) buffer, result);
  sha256_final(&ctx, hash);
  strcpy((char *) block->hash, (char *) hash);

  //createSmallBlocks(buffer, result, block);

  taintMap.push_back(block);
  return block;
}

MemBlock *MemBlock::isOverlapping(const void *buffer, int readSize) {
  unsigned long currentStart = (unsigned long) buffer;
  //unsigned int currentStart = 4294959600; //TESTING FILE-TAINTING
  
  unsigned long currentEnd = currentStart + readSize; //in result is count of read bytes
  
  //printf("curr: %u %u", currentStart, currentEnd);
  
  MemBlock *foundBlock = NULL;
  vector<MemBlock *>::iterator it;
  for (it = taintMap.begin(); it < taintMap.end(); it++) {
    //(*it)->start, (*it)->size, (*it)->fdSrc, fileMap[(*it)->fdSrc]->path.c_str()
    unsigned long comparedStart = (*it)->start;
    unsigned long comparedEnd = (*it)->start + (*it)->size;

    //looking if compared is out of the current
    if ( (comparedStart < currentStart && comparedEnd < currentStart) || (comparedStart > currentEnd && comparedEnd > currentEnd) ) {
      continue; //it is outside, looking for other
    }
    else { //found, it is overlapping
      return *it;
    }
  };
  
  return NULL; //not found
}

void MemBlock::untaint(int fd) {
  
  vector<TaintedFile>::iterator i;
  for (i = taintedFiles.begin(); i < taintedFiles.end(); i++) {
    if (i->path == fileMap[fd]->path) {
      taintedFiles.erase(i);
    }
  }
}

void MemBlock::createSmallBlocks(const void *buffer, int readSize, unsigned long start) {
  for (int i = 0; i < ((int) readSize / SMALL_BLOCK_SIZE); i++) {
    unsigned long newBlockStart = (unsigned long) buffer + i * SMALL_BLOCK_SIZE;
    unsigned long newBlockEnd = newBlockStart + SMALL_BLOCK_SIZE;
    
    SmallBlock *newBlock = new SmallBlock;
    memcpy((void *) newBlock->block, buffer + i * SMALL_BLOCK_SIZE, SMALL_BLOCK_SIZE);
    newBlock->start = start;
    

    smallBlocks.push_back(newBlock);
  }
}

void MemBlock::cutBlock(MemBlock *block, const void *buffer, int size) {
  unsigned long blockStart = (unsigned long) block->start;
  unsigned long blockEnd = (unsigned long) block->start + block->size;
  unsigned long bufferStart = (unsigned long) buffer;
  unsigned long bufferEnd = (unsigned long) buffer + size;
  
  
  if (blockStart < bufferStart && blockEnd > bufferEnd) { //divide into two blocks
    block->size = bufferStart; //trim from right;
    //createSmallBlocks((const void *) block->start, block->size, block);
    
    MemBlock *newBlock = new MemBlock;
    newBlock->start = bufferEnd;
    newBlock->size = blockEnd - bufferEnd;
    newBlock->fdSrc = block->fdSrc;
    newBlock->fpSrc = block->fpSrc;
    //createSmallBlocks((const void *) newBlock->start, newBlock->size, newBlock);
  }
  else if (blockStart > bufferStart && blockEnd < bufferEnd) { //remove block
    //taintMap.erase(block);
  }
  else if (bufferStart > blockStart && bufferStart < blockEnd) { //should be cut from right
    block->size = blockStart;
    //createSmallBlocks((const void *) block->start, block->size, block);
  }
  else if (bufferEnd > blockStart && bufferEnd < blockEnd) { //should be cut from left
    block->start = bufferEnd;
    block->size = block->size - size;
    //createSmallBlocks((const void *) block->start, block->size, block);
  }
  
}

void MemBlock::printAll() {
	vector<MemBlock *>::iterator it;
	for (it = taintMap.begin(); it != taintMap.end(); it++) {

		char path[255];
		//strcpy(path, "(removed)");
		if (fileMap.find((*it)->fdSrc) == fileMap.end()) {
			strcpy(path, "N/A");
		}
		else {
			strcpy(path, fileMap[(*it)->fdSrc]->path.c_str());
		}

		char hash_byte[3];
		char hash[33] = { 0 };
		for (int i = 0; i < 32; i += 2) { //printed all hash
			snprintf(hash_byte, 3, "%02X", (*it)->hash[i]);
			hash[i] = hash_byte[0];
			hash[i + 1] = hash_byte[1];
		}

		log_to_file(LOG_FILE_DEBUG, "start: %lu, size: %d, fd_src: %d, fp_src: %s, hash: %s, smallblocks: ", (*it)->start, (*it)->size, (*it)->fdSrc, path, hash);

		char printedSmallBlock[SMALL_BLOCK_SIZE * 3 + 5];
		vector<SmallBlock *>::iterator it3;
		for (it3 = smallBlocks.begin(); it3 != smallBlocks.end(); it3++) {
			if ((*it3)->start == (*it)->start) {
				buffer_to_string(printedSmallBlock, SMALL_BLOCK_SIZE * 3 + 5, (*it3)->block, SMALL_BLOCK_SIZE);
				log_to_file(LOG_FILE_DEBUG, "%s", printedSmallBlock);
			}
		}
	};
}




void printPrivateFiles() {
  log_to_file(LOG_FILE_DEBUG, "privateFiles: ");
  
  vector<TaintedFile>::iterator it;
  for (it = privateFiles.begin(); it < privateFiles.end(); it++) {
    log_to_file(LOG_FILE_DEBUG, "%s (%llu) ", it->path.c_str(), it->time);
  }
}

void printTaintedFiles() {
  log_to_file(LOG_FILE_DEBUG, "taintedFiles (from fpsDst arrays): ");
  
  vector<TaintedFile>::iterator it;
  for (it = taintedFiles.begin(); it < taintedFiles.end(); it++) {
    log_to_file(LOG_FILE_DEBUG, "%s (%llu) ", it->path.c_str(), it->time);
  }
}



