#include <dlfcn.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string>
#include <string.h>

#include <map>
#include <algorithm>
#include <iostream>
#include <vector>
#include <list>

#include "logging.h"
#include "taint_map.h"

using namespace std;

#define FILENAME_BUFFER_SIZE 1024
char pe_filename_buffer[FILENAME_BUFFER_SIZE] = "";
char pe_pathfile[20];

bool peStartsWith(const char *prefix, const char *string)
{
    size_t prefix_len = strlen(prefix), string_len = strlen(string);
    return string_len < prefix_len ? false : strncmp(prefix, string, prefix_len) == 0;
}

int pe_get_file_name(int fd, char *filename) {
	int result=-1;
	sprintf(pe_pathfile,"/proc/self/fd/%d",fd);
	memset(pe_filename_buffer,'\0', FILENAME_BUFFER_SIZE);
	result = readlink(pe_pathfile, pe_filename_buffer, sizeof(pe_filename_buffer));
	if (result != -1) {
		log_to_file(LOG_FILE_ERROR, "filename for %d is: %s", fd, pe_filename_buffer);
		strcpy(filename, pe_filename_buffer);
	}
	else {
		log_to_file(LOG_FILE_ERROR, "Error finding filename for %d :   %s", fd, strerror(errno));
	}
	return result;
}

const char *print_time() {
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	static char buffer[50];

	sprintf(buffer, "[%d/%d %d:%d:%d] ", tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	return buffer;
}

bool isTaintedFile(string path) {
  vector<TaintedFile>::iterator i;
  for (i = privateFiles.begin(); i < privateFiles.end(); i++) {
    if (i->path == path) {
      return true;
    }
  }
  
  for (i = taintedFiles.begin(); i < taintedFiles.end(); i++) {
    if (i->path == path) {
      return true;
    }
  }
  
  return false;
}
