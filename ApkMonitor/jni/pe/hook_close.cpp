#include <stdio.h>

#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <list>

#include "taint_map.h"
#include "logging.h"
#include "utilities.h"

using namespace std;

void handle_before_close(int fd) {
  log_close(0, fd);

  vector<MemBlock *>::iterator it;
  for (it = taintMap.begin(); it != taintMap.end(); it++) {
    if ((*it)->fdSrc == fd) {
      (*it)->fdSrc = 0; //only marks
    }
  };
 
  //erase entry from fileMap
  map<int, FileInfo *>::iterator it3 = fileMap.find(fd);
  if(it3 != fileMap.end()) {
    fileMap.erase(it3->first);
  }


  make_snapshot("AFTER before_close");
}

void handle_after_close(int result, int fd) {
	//log_close(result, fd);
}
