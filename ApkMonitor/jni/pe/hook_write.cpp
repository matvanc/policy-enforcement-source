#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <list>

#include "taint_map.h"
#include "logging.h"
#include "utilities.h"
#include "sha256.h"

using namespace std;

void handle_before_write(int file, const void *buffer, size_t count) {
	//log_write(0, file, buffer, count);
}

void handle_after_write(int result, int file, const void *buffer,
		size_t count) {
	log_write(result, file, buffer, count);

	unsigned long currentStart = (unsigned long) buffer;
	//unsigned int currentStart = 4294959600; //TESTING FILE-TAINTING

	unsigned long currentEnd = currentStart + result; //in result is count of read bytes

	//printf("curr: %u %u", currentStart, currentEnd);

	bool isTaintedBlock = false; //if found at least one overlapping block, overall block is tainted
	MemBlock *foundBlock = NULL;
	vector<MemBlock *>::iterator it;
	/*
	 for (it = taintMap.begin(); it < taintMap.end(); it++) {
	 //(*it)->start, (*it)->size, (*it)->fdSrc, fileMap[(*it)->fdSrc]->path.c_str()
	 unsigned long comparedStart = (*it)->start;
	 unsigned long comparedEnd = (*it)->start + (*it)->size;

	 //looking if compared is out of the current
	 if ( (comparedStart < currentStart && comparedEnd < currentStart) || (comparedStart > currentEnd && comparedEnd > currentEnd) ) {
	 continue; //it is outside, looking for other
	 }
	 else { //found, it is overlapping
	 isTaintedBlock = true;

	 if ( !TaintedFile::foundInVector(fileMap[file]->path, &taintedFiles) ) { //adding using file-tainting
	 taintedFiles.push_back(TaintedFile::create(fileMap[file]->path));
	 }
	 }
	 };*/

	  if (fileMap.find(file) == fileMap.end()) { //cant continue if this is not valid value
	 	  return;
	   }

	if (!isTaintedBlock) { //even the file should not be tainted? (hash checking)
		unsigned char hash[32];
		SHA256_CTX ctx;
		sha256_init(&ctx);
		sha256_update(&ctx, (unsigned char *) buffer, result);
		sha256_final(&ctx, hash);

		vector<MemBlock *>::iterator it;
		for (it = taintMap.begin(); it != taintMap.end(); it++) { //searching in hash database
			if (memcmp(hash, (*it)->hash, 32) == 0) { //strings are equal
				MemBlock *block = (*it);

				if (!TaintedFile::foundInVector(fileMap[file]->path, &taintedFiles)) { //adding using data-tainting
					taintedFiles.push_back(TaintedFile::create(fileMap[file]->path));
				}

				isTaintedBlock = true;
			}
		}
	}

	if (!isTaintedBlock) { //we have untainted memory block after all (on the file-level and also data-level)
		FileInfo *fInfo = fileMap[file];
		int oflag = fInfo->flags;
		int oflag2 = oflag | O_APPEND;
		bool inAppendMode = oflag > oflag2;
		if (isTaintedFile(fInfo->path) && !inAppendMode) {
			MemBlock::untaint(file);
		}
	}
}
