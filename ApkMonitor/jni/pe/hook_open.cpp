#include <stdio.h>

#include <map>
#include <vector>
#include <string>
#include <list>

#include "taint_map.h"
#include "logging.h"
#include "utilities.h"

void handle_before_open(const char *file, int flags, int mode) {
	//log_open(0, file, flags, mode);
}

void handle_after_open(int result, const char *file, int flags, int mode) {
	log_open(result, file, flags, mode);

	FileInfo::add(result, file, flags, mode);
	

	make_snapshot("AFTER after_open");

}
