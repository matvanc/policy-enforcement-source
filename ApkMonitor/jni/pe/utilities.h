#ifndef PE_UTILITIES_H_
#define PE_UTILITIES_H_

bool peStartsWith(const char *prefix, const char *string);

int pe_get_file_name(int fd, char *filename);

const char *print_time();

bool isTaintedFile(std::string path);

#endif /* PE_UTILITIES_H_ */
